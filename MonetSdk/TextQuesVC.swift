//
//  TextQuesVC.swift
//  MonetSDK
//
//  Created by Monet  on 08/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class TextQuesVC: ViewControllerSdk {

    let surveyCntr = SurveyController.shared
    var questData: Quest?
    let placeHolderText = "Say something"
    let placeholderColor = UIColor.gray
    let mainTextColor = UIColor.black

    
    @IBOutlet weak var qNumLabel: UILabel!
    @IBOutlet weak var questStatementLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var ansTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        ansTextView.delegate = self
        hideKeyboardWhenTappedAround()
        addKeyboardNotification()
        
        handleData()
        setUpViews()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        guard let nxtVC = surveyCntr.nextSurveyVC() else {
            showThankyouPage()
            return
        }
        
        (self.questData?.answer as! AnsText).ansStringValue = ansTextView.text
        
       showNxtVC(nxVC: nxtVC)
    }
    
    override func backAction() {
        if surveyCntr.isPreActive{
            surveyCntr.preIndex -= 1
            self.navigationController?.popViewController(animated: true)
        } else{
            surveyCntr.postIndex -= 1
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func enableDisableProceedBtn() {
     
        nextBtn.isEnabled = false
    }
 
}

extension TextQuesVC{
    
    func setUpViews(){
        if SurveyController.shared.isPreActive{
            qNumLabel.text = "Q.\(SurveyController.shared.preIndex)"
        } else{
            qNumLabel.text = "Q.\(SurveyController.shared.postIndex)"
        }
        nextBtn.isEnabled = false
        nextBtn.setTitleColor(UIColor(rgbString: ColorType.disableBtn.rawValue), for: .disabled)
        nextBtn.setTitleColor(.white, for: .normal)
        nextBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
        
        questStatementLabel.textColor = .white
        
        ansTextView.tintColor = .black
        ansTextView.text = placeHolderText
        ansTextView.textColor = placeholderColor
    }
    
    func handleData(){
        if  surveyCntr.isPreActive{
            self.questData = surveyCntr.surveyContainer.preSurvey[surveyCntr.preIndex]
            self.questStatementLabel.text = self.questData?.statement
            self.surveyCntr.preIndex += 1
        }else{
            self.questData = surveyCntr.surveyContainer.postSurvey[surveyCntr.postIndex]
            self.questStatementLabel.text = self.questData?.statement
            self.surveyCntr.postIndex += 1
        }
    }
    
    func showThankyouPage(){
        print("-----no vc-----")
        let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNxtVC(nxVC: String){
        let vc = storyboard!.instantiateViewController(withIdentifier: nxVC)
        navigationController?.pushViewController(vc, animated: true)
    }
    
 
}

extension TextQuesVC{
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func addKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func keyBoardWillShow(sender: Notification){
        let frameInfo = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        self.view.frame.origin.y = -frameInfo.height
    }
    
    @objc func keyBoardWillHide(sender: NotificationCenter){
        self.view.frame.origin.y = 0
    }
}

extension TextQuesVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView){
        if ansTextView.textColor == placeholderColor{
            DispatchQueue.main.async {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
    
//    func textViewDidEndEditing(_ textView: UITextView){
//        if ansTextView.text == "" {
//            ansTextView.text = placeHolderText
//            ansTextView.textColor = placeholderColor
//        }
//    }
    
    func textViewDidChange(_ textView: UITextView){
        if ansTextView.textColor == placeholderColor{
            DispatchQueue.main.async {
                self.ansTextView.text.removeSubrange(self.ansTextView.text.index(after: self.ansTextView.text.startIndex)..<self.ansTextView.text.endIndex)
                self.ansTextView.textColor = self.mainTextColor
            }
        } else if ansTextView.text == "" {
            ansTextView.text = placeHolderText
            ansTextView.textColor = placeholderColor
            DispatchQueue.main.async {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        } else if textView.text.count >= 5 {
//            textView.textColor = .black
            self.nextBtn.isEnabled = true
        } else{
//            textView.textColor = .gray
            self.nextBtn.isEnabled = false
        }
    }

}
