//
//  Api.swift
//  MonetSDK
//
//  Created by Monet  on 25/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import Foundation

enum ApiMethod: String {
    case post = "post"
    case get = "get"
}

struct ApiRequest {
//    private let baseUrl = "http://192.168.1.205:8002/api/"
//    private let baseUrl = "https://dev.monetrewards.com/MonetSDK/api/"
//    private let baseUrl = "http://192.168.1.114:8125/api/" // By Surender (Local)
//    private let baseUrl = "https://dev.monetrewards.com/MonetSdk/api/" // By Surender (Live)
    private var baseUrl = "http://app.monetrewards.com/MonetSdk/api/" // By Surender new server (Live)

//    private let baseUrl = "https://dev.monetrewards.com/MonetSDK/api/"
    private var method = ApiMethod.post
    private var endUrl = ""
    private var parameters: [String:String]?
    private var body: Data?
    private let session = URLSession.shared
    private var authenticationToken: String? = nil
    private var otherHeadersDic: [String:String]? = nil
    
    
    init(type: ApiMethod, endUrl: String, para: [String:String]?, body: Data?, authToken: String?, otherHedersKeyValue: [String:String]?){
        self.method = type
        self.endUrl = endUrl
        self.parameters = para
        self.body = body
        self.authenticationToken = authToken
        self.otherHeadersDic = otherHedersKeyValue
    }
    
    init(downloadImage Url: URL, authToken: String?, completion: @escaping (Data?) ->Void){
        self.method = .get
        self.authenticationToken = authToken
        var request = URLRequest(url: Url)
        if let token = self.authenticationToken{
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        if let header = otherHeadersDic{
            let a = header.keys
            let b = a.first
            request.setValue(header[b!]!, forHTTPHeaderField: b!)
        }
        
      
            request.httpMethod = method.rawValue
    
        let dataTask = session.dataTask(with: request) { (data, response, err) in
            //            print(response)
            //            print(data)
            //            print(err)
            if let responseData = data{
                completion(responseData)/*-------------Escaping: Since this its execution time will be depended upong the "dataTask's" thread--------------*/
            } else {
                completion(nil)
            }
        }
        dataTask.resume()
    }

    func hit(completion: @escaping (Data?) ->Void )  {
        var urlStr = baseUrl + endUrl
        var request = URLRequest(url: URL(string: urlStr)!)
        if let token = self.authenticationToken{
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        if let param = parameters{
            urlStr += append(parametersToUrl: param)
        }
//        request.allHTTPHeaderFields = otherHeadersDic
        if let header = otherHeadersDic{
            let a = header.keys
            let b = a.first
            request.setValue(header[b!]!, forHTTPHeaderField: b!)
        }

        switch method {
        case .get:
            request.httpMethod = method.rawValue
        case .post:
            request.httpMethod = method.rawValue
//            request.allHTTPHeaderFields = ["Content-Type":"application/json"]
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            if let body = body{
                request.httpBody = body
            }
            else {
                fatalError("*** this post api doesn't have any valid body")
            }
        }
        let dataTask = session.dataTask(with: request) { (data, response, err) in
            print(response)
            print(data)
            print(err)
            if let responseData = data{
                completion(responseData)
            } else {
                completion(nil)
            }
        }
        dataTask.resume()
    }
    

    
    
    private func append(parametersToUrl Dic: [String:String]) -> String{
        var counter = Dic.count
        var postUrl = "?"
        
        for (key, value) in Dic{
            postUrl += key
            postUrl += "="
            let newValue = value.components(separatedBy: CharacterSet.whitespaces).joined(separator: "%20")
            postUrl += newValue
            
            counter -= 1
            if counter > 0{
                postUrl += "&"
            }
        }
        return postUrl
    }
    
    private func jsonConvertor(bodyOfHttpRequest rawBody: Any) -> Data{
//        var data = Data()
//        do {
            let data = try! JSONSerialization.data(withJSONObject: rawBody, options: .prettyPrinted)
            print(data)
//        } catch{
//            print(error)
//        }
        return data
    }
    
//    private func append(headerToRequest headerDic: [String:String]){
//
//    }

    
}
