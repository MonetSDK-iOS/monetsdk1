//
//  DataCreation.swift
//  MonetSDK
//
//  Created by Monet  on 07/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

/*--------------------------------------------------------------------
 Controls survey flow
 Keeps track of upcoming surveys & viewCntrls
 --------------------------------------------------------------------*/

class SurveyController{
    
    static var shared = SurveyController()
    var capaignLaunched: Bool? = nil
    let ansSender = AnswerSender()
    private var jsonData = Data()
    private let decoder = JSONDecoder()
    private var apiData: ApiDataModel!
    var surveyContainer: SurveyContainer!
    var launchedSruveys: [SurveySequence:Bool] = [.pre:false, .post:false]
    var surveyIndex = 0
    var preIndex = 0
    var postIndex = 0
    var isPreActive = false
    var isEmotionActive = false
    var surveyCount: Int { get { return self.surveyContainer.sequence.count}}
    var preQuestCount: Int { get { return self.surveyContainer.preSurvey.count}}
    var postQuestCount: Int { get { return self.surveyContainer.postSurvey.count}}
    
  
    
    /*--------------------------------------------------------------------------------
     Initialiser
     ---------------------------------------------------------------------------------*/
    init() {
        // ---- Adds observer to handle staging on app kill
        NotificationCenter.default.addObserver(self, selector: #selector(sendStagingOnAppTermintion(_:)), name: UIApplication.willTerminateNotification, object: nil)
    }
    
    
    
    /*--------------------------------------------------------------------------------
     Staging
     Update the database how many stages of the campaign has been completed.
     ---------------------------------------------------------------------------------*/
    
    //  --- Handles Staging with notification on app kill
    @objc private func sendStagingOnAppTermintion(_: Notification){
        print("app termination notification func called")
            guard capaignLaunched ?? false else{return}
//        guard currentStageKey != 0 else {return}
//        SurveyController.shared.stagingData[String(SurveyController.shared.currentStageKey)] = 3 //---Sets the stage where user exits the campaign.
//            stagingApi()
    }
    
    
    // ---- Staging: Updates staging from VCs
//    func stageUpdate(currentKey key: Int, value: Int, hitApiNow: Bool){
//        SurveyController.shared.currentStageKey = key
//        SurveyController.shared.stagingData[String(key)] = value
//        guard hitApiNow else {return}
//    }

    // --- Api: Hit staging api
//    func <#name#>(<#parameters#>) -> Void {
//        <#function body#>
//    }
    func sendStagingApi(isCampaignCompleted: Bool){
//        let
//        let body: Data = {
//            let foundataionObj = SurveyController.shared.surveyContainer.stagingData
//            let data = try! JSONEncoder().encode(foundataionObj)
//            return data
//        }()
        let complete: Int = {
            if isCampaignCompleted{
                if SurveyController.shared.surveyContainer.sequence.count == 4{
                    return 3
                } else{
                    if SurveyController.shared.surveyContainer.sequence.contains(SurveySequence.reaction){
                        return 1
                    }
                    return 2
                }
            }
            return 0
        }()
        
        let parameters = [ "success_flag":complete]
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let api = ApiRequest(type: .post, endUrl: ApiEndPoinds().staging, para:nil , body: data, authToken: SurveyController.shared.surveyContainer.tokenForEndUser
            , otherHedersKeyValue: ["licence-key" : AppInjectedData.shared.licence])
        api.hit { (data) in
            print(data)
        }
    }
}

extension SurveyController{
    /*-------------------------------------------
     Next VC
     Save Ans : By calling "AnsSender"  class
     --------------------------------------------*/

    // --- Returns vc name for upcoming survey
    func nextSurveyVC() -> String? {
        guard surveyCount > 0 && surveyIndex < surveyCount else {return nil}/*---Check for Empty survey & survey Index out of range---*/
      
        switch  self.surveyContainer.sequence[surveyIndex]/*---Checks for squence type---*/  {
        case .pre:
            isPreActive = true//--- Flag to know survey type (i.e Pre and Post) ---
            if launchedSruveys[.pre] == false/*---Used only when Pre/Post launced---*/{
                launchedSruveys[.pre] = true
                return viewCntrlIdentiers.QuestDisclaimerPageVC.rawValue
            }
            return handlePreCase() //---Tells which quest screen should be opened next
        case .post:
            isPreActive = false
            if launchedSruveys[.post] == false{
                launchedSruveys[.post] = true
                return viewCntrlIdentiers.QuestDisclaimerPageVC.rawValue
            }
            return handlePostCase()
        case .emotion:
            isEmotionActive = true//---current survey flag---
            surveyIndex += 1
            return viewCntrlIdentiers.VideoDisclaimerPageVC.rawValue
        case .reaction:
            isEmotionActive = false
            surveyIndex += 1
            return viewCntrlIdentiers.VideoDisclaimerPageVC.rawValue
        }
    }
    
    

    private func handlePreCase() -> String?{
        guard preQuestCount > 0 else { return nil} // --- Empty: Pre Survey quest array is empty
        if  preIndex < preQuestCount{
            return getQVcName(qType: self.surveyContainer.preSurvey[preIndex].type)
        } else{
            // --- Finished: Questions are finished as index is out of range now.
            ansSender.processSurvey(surveyType: .pre)// --- Api: Save Answers
            surveyIndex += 1
            return nextSurveyVC()
        }
    }
    
    private func handlePostCase() -> String?{
        guard postQuestCount > 0 else {return nil} // --- Empty: Post Survey quest array is empty
        if postIndex < postQuestCount{
            return getQVcName(qType: self.surveyContainer.postSurvey[postIndex].type)
        } else{
            ansSender.processSurvey(surveyType: .post)// --- Api: Save Answers
            surveyIndex += 1
            return nextSurveyVC()
        }
    }
    
    private func getQVcName(qType: QuestType) -> String?{
        switch qType {
        case .grid:
            return viewCntrlIdentiers.GridQuesVC.rawValue
        case .single:
            return viewCntrlIdentiers.RadioQuesVC.rawValue
        case .multiple:
            return viewCntrlIdentiers.CheckboxQuesVC.rawValue
        case .numeric:
            return viewCntrlIdentiers.RatingQuesVC.rawValue
        case .freetext:
            return viewCntrlIdentiers.TextQuesVC.rawValue
//        case .logicalWithChildQuest:
//            return viewCntrlIdentiers.RatingQuesVC.rawValue
        case .image:
            return viewCntrlIdentiers.ImageRadioQuesVC.rawValue
        case .multiple_image:
            return viewCntrlIdentiers.ImageCheckBoxQuestVC.rawValue
//        case .undefined:
//            return viewCntrlIdentiers.RatingQuesVC.rawValue
        }
    }
    
    
    

}

