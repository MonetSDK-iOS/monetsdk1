//
//  File.swift
//  MonetSdk
//
//  Created by Monet  on 03/05/19.
//

import Foundation


func getImage (name: String) -> UIImage {
    var image = UIImage()
    if let img = UIImage(named: name, in: Bundle(for: MonetSdkLauncher.self), compatibleWith: nil){
        image = img
    } else {
        print("----- Image resource missing with name '\(name)' ------")
        fatalError()
    }
    return image
}
