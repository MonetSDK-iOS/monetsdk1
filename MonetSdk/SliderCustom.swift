//
//  SliderCustom.swift
//  MonetSDK
//
//  Created by Monet  on 13/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

@IBDesignable

 class SliderCustom: UISlider {

    @IBInspectable var trackThickness: CGFloat = 2
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let rect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width , height: trackThickness)
        return rect
    }
    

}
