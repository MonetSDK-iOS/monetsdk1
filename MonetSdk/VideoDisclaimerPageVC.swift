//
//  EmotionDisclaimerPageVC.swift
//  MonetSDK
//
//  Created by Monet  on 09/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class VideoDisclaimerPageVC: ViewControllerSdk {
    
    let surveyCntr = SurveyController.shared
//    private var stagingValue: String{
//        get{
//            if SurveyController.shared.isEmotionActive{
//                return 4
//            }
//            return 5
//        }
//    }
    
    @IBOutlet weak var thumbnailImgView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var smallImageView: UIImageView!
    @IBOutlet weak var videoNameLabel: UILabel!
    @IBOutlet weak var instuctLabel: UILabel!
    @IBOutlet weak var proceedBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        setUpViews()
        getThumbnail()
    }
    
    /* --------------------------------------------------------
     Staging
     --------------------------------------------------------*/
    override func viewDidAppear(_ animated: Bool) {
        if SurveyController.shared.isEmotionActive{
            SurveyController.shared.surveyContainer.stagingData.detection = ["q-card":stageStates.start]
        } else{
            SurveyController.shared.surveyContainer.stagingData.reaction = ["q-card":stageStates.start]
        }
        SurveyController.shared.sendStagingApi(isCampaignCompleted: false)
    }
    
    @IBAction func proceedAction(_ sender: UIButton) {
        let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.InstructionsPageVC.rawValue)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /* --------------------------------------------------------
     Staging
     If back btn pressed alert asks to quit or continue the survey
     If quits Staging api will be hit
     --------------------------------------------------------*/
    override func backAction() {
        self.quitAcion(quit: {
            if SurveyController.shared.isEmotionActive{
                SurveyController.shared.surveyContainer.stagingData.detection = ["q-card":stageStates.exit]
            } else{
                SurveyController.shared.surveyContainer.stagingData.reaction = ["q-card":stageStates.exit]
            }
            SurveyController.shared.sendStagingApi(isCampaignCompleted: false)
        }, cancel: nil)
    }
    
    func setUpViews(){
        
        smallImageView.layer.cornerRadius = smallImageView.frame.height/2
        smallImageView.clipsToBounds = true
        smallImageView.backgroundColor = .gray
        
        timeLabel.text = SurveyController.shared.surveyContainer.videoDuration+" Mins"
        proceedBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        proceedBtn.layer.cornerRadius = proceedBtn.frame.height/2
        smallImageView.layer.cornerRadius = smallImageView.frame.height/2
        smallImageView.clipsToBounds = true
    }
    
    func getThumbnail () {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async {
            _ = ApiRequest(downloadImage: SurveyController.shared.surveyContainer.videoThumbnailUrl!, authToken: nil) { (imgData) in
                guard let data = imgData else {
                    return
                }
                guard let img = UIImage(data: data) else {
                    return
                }
                DispatchQueue.main.async {
                    self.thumbnailImgView.image = img
                    //                    self.videoImgView.image = img
                }
            }
        }
    }
    
}


