//
//  ConstantsForSdk.swift
//  MonetSDK
//
//  Created by Monet  on 27/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//


struct ApiEndPoinds {
    let lauchPad = "sdk"
    let savePrePostAns = "storeFeedback"
//    let staging = "feedbackStage"
    let saveReactions = "reactionCommentData"
    let staging = "updatePageStage"
}



enum ColorType: String {
    case topColor = "#192557"
    case middleColor = "#664EA1"
    case bottomThemeColor = "#57217A"
    case blueDarkColor = "#1A2557"
    case placeHoder = "#A1A1A1"
    case yellow = "#FFD500"
    case greenLight = "#489D09"
    case gray = "#707070"
    case red = "#FF1010"
    case disableBtn = "#5F398F"
}

enum viewCntrlIdentiers: String{
    case LandingPageVC
    case VideoDisclaimerPageVC
    case InstructionsPageVC
    case ThankyouPageVC
    
    case QuestDisclaimerPageVC
    case CheckboxQuesVC
    case RadioQuesVC
    case TextQuesVC
    case RatingQuesVC
    case GridQuesVC
    case ImageRadioQuesVC
    case ImageCheckBoxQuestVC
    
    case FaceAdjustmentPageVC
    case EmotionPlayerVC
    
    case ReactionPlayerVC
}
