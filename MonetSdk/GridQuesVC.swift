//
//  GridQuesVC.swift
//  MonetSDK
//
//  Created by Monet  on 08/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class GridQuesVC: ViewControllerSdk {

    let surveyCntr = SurveyController.shared
    var questData: Quest?
    
    @IBOutlet weak var qNumLabel: UILabel!
    @IBOutlet weak var questStatementLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageCntl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleData()
        setUpViews()

        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        guard let vcId = surveyCntr.nextSurveyVC() else {
            print("-----no vc-----")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = storyboard!.instantiateViewController(withIdentifier: vcId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func backAction() {
        if surveyCntr.isPreActive{
            surveyCntr.preIndex -= 1
            self.navigationController?.popViewController(animated: true)
        } else{
            surveyCntr.postIndex -= 1
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpViews(){
        if SurveyController.shared.isPreActive{
            qNumLabel.text = "Q.\(SurveyController.shared.preIndex)"
        } else{
            qNumLabel.text = "Q.\(SurveyController.shared.postIndex)"
        }
        nextBtn.isHidden = true
        nextBtn.backgroundColor = .clear
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
    }
    
    override func viewDidLayoutSubviews() {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize.width = collectionView.frame.width
        layout.itemSize.height = collectionView.frame.height
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        collectionView.isPagingEnabled = true
    }
  
    
    func handleData(){
        if  surveyCntr.isPreActive{
            questData = surveyCntr.surveyContainer.preSurvey[surveyCntr.preIndex]
            self.surveyCntr.preIndex += 1
        }else{
            questData = surveyCntr.surveyContainer.postSurvey[surveyCntr.postIndex]
            self.surveyCntr.postIndex += 1
        }
        self.questStatementLabel.text = questData?.statement
     
        pageCntl.numberOfPages = questData?.optionAry.count ?? 0
        
    }

}

extension GridQuesVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questData?.optionAry.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollectCell", for: indexPath) as! GridCollectCell
        cell.subQuetLabel.text = questData?.optionAry[indexPath.item].statement
        cell.questNumLabel.text = "Question \(indexPath.item + 1)"
        cell.tableView.dataSource = self
        cell.tableView.delegate = self
        cell.tableView.tag = indexPath.item
        cell.tableView.reloadData()
        cell.backgroundColor = .clear
    
        print("cellTag\(cell.tableView.tag) = ItemNO\(indexPath.item)")
        return cell
    }

}

extension GridQuesVC: UICollectionViewDelegate{
    
    //----Controls page control and next btn according to visibel cell-----
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        let currentPageNum = Int((collectionView.visibleCells[0].frame.origin.x)/(collectionView.frame.size.width))
        pageCntl.currentPage = currentPageNum
        
//        if currentPageNum == pageCntl.numberOfPages-1{
//            self.nextBtn.isHidden = false
//        } else{
//            self.nextBtn.isHidden = true
//        }
    }
    
}

extension GridQuesVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questData!.optionAry[tableView.tag].gridOptionArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell")!
        guard let subOptArry = questData?.optionAry[tableView.tag].gridOptionArray else{
            return cell
        }
        
        print("q num = \(tableView.tag), selected ans = \(questData?.optionAry[tableView.tag].subAnswer.selectedOption) & table row = \(indexPath.row)")
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = subOptArry[indexPath.row].statement
        if questData?.optionAry[tableView.tag].subAnswer.selectedOption == indexPath.row{
//            cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "checkMark"))
            cell.accessoryView = UIImageView(image: getImage(name: "checkMark"))

            cell.textLabel?.textColor = UIColor(rgbString: ColorType.yellow.rawValue)
            cell.textLabel?.textColor = .white
            cell.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue, alpha: 0.2)
            cell.textLabel?.backgroundColor = .clear
        } else {
            cell.accessoryView = UIImageView()
            cell.textLabel?.textColor = .white
            cell.backgroundColor = .clear
        }
        return cell
    }
    
}

extension GridQuesVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let opt = questData?.optionAry[tableView.tag] else{
//            return
//        }
       let opt = questData!.optionAry[tableView.tag]
        opt.subAnswer.selectedOption = indexPath.row
        let ans = questData?.answer as! AnsGrid
        ans.selectedAnsArry[tableView.tag] = indexPath.row
        if ans.selectedAnsArry.count == questData?.optionAry.count{
            self.nextBtn.isHidden = false
        }
        print("q num = \(tableView.tag), selected ans = \(questData?.optionAry[tableView.tag].subAnswer.selectedOption) & table row = \(indexPath.row)")
        tableView.reloadData()
    
    }
}
