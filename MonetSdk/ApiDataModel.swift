//
//  ApiDataModel.swift
//  MonetSDK
//
//  Created by Monet  on 01/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

/*---------------------------------------------------------------------------------
Parsing json from api
 -----------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------
 NOTE: Do not change any property's name. Essensial for mapping with api keys
 -----------------------------------------------------------------------------------*/
struct ApiDataModel: Codable {
    
    let error: Bool
    let code: Int
    let message: String
    let data: ApiData?
}
//    let code: Int
//    let Message: String
//    let data: ApiData?

/*---------------------------------------------------------------------------------
 NOTE: Do not change any property's name. Essensial for mapping with api keys
 -----------------------------------------------------------------------------------*/

struct ApiData: Codable {
    let content_type: String?
    let content_length: Int?
    let content_url: String
    let thumb_url: String
    let cmp_id: Int
    let sequence: [SurveySequence]
    let reaction_inputs: [EmojiEncoding]?
    let pre: ApiQuestions?
    let post: ApiQuestions?
    let user_response_id: Int
    let token: String
    let cmp_name: String?
}

/*---------------------------------------------------------------------------------
 NOTE: Do not change any property's name. Essensial for mapping with api keys
 -----------------------------------------------------------------------------------*/

struct ApiQuestions: Codable {
    let questions: [ApiQuestDetails]?
    
}

/*---------------------------------------------------------------------------------
 NOTE: Do not change any property's name. Essensial for mapping with api keys
 -----------------------------------------------------------------------------------*/

struct ApiQuestDetails: Codable {
    let qs_id: Int
    let question: String
    let questionType: QuestType
    let options: [ApiOptions]?
    let values: [ApiValues]?
}


/*---------------------------------------------------------------------------------
 NOTE: Do not change any property's name. Essensial for mapping with api keys
 -----------------------------------------------------------------------------------*/

struct ApiOptions: Codable {
    let opt_id: Int
    let option_value: String
    let type: OptionValueType
}

/*---------------------------------------------------------------------------------
 NOTE: Do not change any property's name. Essensial for mapping with api keys
 -----------------------------------------------------------------------------------*/

struct ApiValues: Codable {
    let grid_id: Int
    let grid_value: String
    
    enum CodingKeys: String, CodingKey {
        case grid_id = "gr_id"
        case grid_value = "grid_value"

    }
}

enum OptionValueType: String, Codable{
    case none
    case others
    case freetext
    case normal
    case image
}


