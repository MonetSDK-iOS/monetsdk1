//
//  CheckboxQuesVC.swift
//  MonetSDK
//
//  Created by Monet  on 06/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class CheckboxQuesVC: ViewControllerSdk {

    private var stagingKey: Int{
        get{
            if SurveyController.shared.isPreActive{
                return 2
            }
            return 3
        }
    }
    let surveyCntr = SurveyController.shared
    var questData: Quest?

    @IBOutlet weak var qNumLabel: UILabel!
    @IBOutlet weak var questStatementLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        handleData()
        setUpViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        #warning("AKASH DO IT NOW: staging was here")
//        SurveyController.shared.stageUpdate(currentKey: self.stagingKey, value: 4, hitApiNow: false)
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        guard let nxtVC = surveyCntr.nextSurveyVC() else {
//            print("-----no vc-----")
            showThankyouPage()
             return
        }
        showNxtVC(nxVC: nxtVC)
    }
    
    func enableDisableProceedBtn(){
        for (_, value) in (self.questData?.answer as! AnsCheckBox).selectedOptions{
            if  value{
                nextBtn.isEnabled = true
                return
            }
        }
        nextBtn.isEnabled = false
        return
    }
    
    override func backAction() {
        if surveyCntr.isPreActive {
            surveyCntr.preIndex -= 1
            self.navigationController?.popViewController(animated: true)
        } else{
            surveyCntr.postIndex -= 1
            self.navigationController?.popViewController(animated: true)
        }
    }
}


extension CheckboxQuesVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.questData?.optionAry.count else {
            return 0
        }
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let option = questData?.optionAry[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = option?.statement
        if (self.questData?.answer as! AnsCheckBox).selectedOptions[indexPath.row] == true{
            cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "checkMark"))
            cell.textLabel?.textColor = UIColor(rgbString: ColorType.yellow.rawValue)
            cell.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue, alpha: 0.2)
            cell.textLabel?.backgroundColor = .clear

        } else{
            cell.accessoryView = nil
            cell.textLabel?.textColor = .white
            cell.backgroundColor = .clear
        }
        return cell
    }

}



extension CheckboxQuesVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let checkBoxAns = self.questData?.answer as! AnsCheckBox
        checkBoxAns.selectedOptions[indexPath.row] = !checkBoxAns.selectedOptions[indexPath.row]!
        if checkBoxAns.selectedOptions[indexPath.row] == true{
//            cell?.accessoryView = UIImageView(image: #imageLiteral(resourceName: "checkMark"))
            cell?.accessoryView = UIImageView(image: UIImage(named: "checkMark", in: Bundle(for: MonetSdkLauncher.self), compatibleWith: nil))
            cell?.textLabel?.textColor = UIColor(rgbString: ColorType.yellow.rawValue)
            cell?.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue, alpha: 0.2)
            cell?.textLabel?.backgroundColor = .clear
        } else{
            cell?.accessoryView = nil
            cell?.textLabel?.textColor = .white
            cell?.backgroundColor = .clear
        }
        self.enableDisableProceedBtn()
    }
}

extension CheckboxQuesVC{
    
    func setUpViews(){
        if SurveyController.shared.isPreActive{
            qNumLabel.text = "Q.\(SurveyController.shared.preIndex)"
        } else{
            qNumLabel.text = "Q.\(SurveyController.shared.postIndex)"
        }
        nextBtn.isEnabled = false
        nextBtn.setTitleColor(UIColor(rgbString: ColorType.disableBtn.rawValue), for: .disabled)
        nextBtn.setTitleColor(.white, for: .normal)
        nextBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
    }
    
    func handleData(){
        if  surveyCntr.isPreActive{
            questData = surveyCntr.surveyContainer.preSurvey[surveyCntr.preIndex]
            self.surveyCntr.preIndex += 1
        }else{
            questData = surveyCntr.surveyContainer.postSurvey[surveyCntr.postIndex]
            self.surveyCntr.postIndex += 1
        }
        self.questStatementLabel.text = questData?.statement

    }
    
    func showThankyouPage(){
        print("-----no vc-----")
        let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNxtVC(nxVC: String){
        let vc = storyboard!.instantiateViewController(withIdentifier: nxVC)
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
