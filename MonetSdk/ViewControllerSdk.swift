//
//  ViewControllerSdk.swift
//  MonetSDK
//
//  Created by Monet  on 26/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

  class ViewControllerSdk: UIViewController {
   
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
    }
    
    private func setUpView(){
        
        self.view.tintColor = .white
        
        //----------This layer nil if top view class of vc in storyboard is not changed to "UIViewGradient" class --------
        let layer = view.layer as? CAGradientLayer
        layer?.colors = [UIColor(rgbString: ColorType.topColor.rawValue).cgColor, UIColor(rgbString: ColorType.middleColor.rawValue).cgColor, UIColor.init(rgbString: ColorType.bottomThemeColor.rawValue).cgColor]
        
        //----------Make nav bar transparent by empty img--------
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //---------- Remove seprator line from navbar by empty img --------
        navigationController?.navigationBar.shadowImage = UIImage()
     
        //---------- Add back button image --------
        addBackBtn()
        
        navigationItem.titleView = UIImageView(image: AppInjectedData.shared.appLogoImg)
        
        //----------Checks which theme is selectd by client--------
        switch AppInjectedData.shared.sdkTheme {
        case .gray:
            self.view.backgroundColor = .darkGray
        case .monet:
            //----------Colors set in UIViewGradient class--------
            print("monet theme selected")
        }
    }
    
    //----------Overide this in subclass and handle back btn click--------
    @objc private func handleBackBtnClick(_ btn: UIButton){
        backAction()
    }
    
    
    func backAction(){
        navigationController?.popViewController(animated: true)
    }
    

    
    func addBackBtn(){
        //----------Add back btn to nav bar--------
//        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backArrow")!, style: .done, target: self, action: #selector(handleBackBtnClick(_:)))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getImage(name: "backArrow"), style: .done, target: self, action: #selector(handleBackBtnClick(_:)))

       
    }
    
    func checkForTheme(){

    }
    
    func quitAcion(quit: (() -> Void)?, cancel: (() -> Void)?){
        let alertCntr = UIAlertController(title: "Warning!", message: "Quiting survey will affect your rewards. \n Still want to quit?", preferredStyle: .actionSheet)
        let quitAction = UIAlertAction(title: "Quit", style: .destructive){ (uiAlerAction) in
            self.dismiss(animated: true, completion: quit)
        }
        let cancelAction = UIAlertAction(title: "Continue", style: .cancel){ (uiAlerAction) in
            if let action = cancel{
                action()
            }
        }
        alertCntr.addAction(quitAction)
        alertCntr.addAction(cancelAction)
        self.present(alertCntr, animated: true, completion: nil)
    }
    
    
//    func setButton(title: String){
//        self.bottomBtn = UIButton()
//        view.addSubview(bottomBtn!)
//        let height: CGFloat = 48
//        bottomBtn!.translatesAutoresizingMaskIntoConstraints = false
//        bottomBtn!.heightAnchor.constraint(equalToConstant: height).isActive = true
//        bottomBtn!.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
//        bottomBtn!.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
//        bottomBtn!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -24).isActive = true
//        bottomBtn!.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
//        bottomBtn!.layer.cornerRadius = height/2
//
//        let titleAttrbStr = NSAttributedString(string: title, attributes: [.font : UIFont(name: "Roboto-Regular", size: 14)!, .foregroundColor : UIColor.white])
//        bottomBtn!.setAttributedTitle(titleAttrbStr, for: .normal)
//    }
    

}
