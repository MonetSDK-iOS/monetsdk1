//
//  RadioQuesVC.swift
//  MonetSDK
//
//  Created by Monet  on 08/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class RadioQuesVC: ViewControllerSdk {

    let surveyCntr = SurveyController.shared
    var questData: Quest?
    
    @IBOutlet weak var qNumLabel: UILabel!
    @IBOutlet weak var questStatementLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
//        tableView.rowHeight = 100
        handleData()
        setUpViews()

    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        
        
        
        
        
        guard let nxtVC = surveyCntr.nextSurveyVC() else {
            print("-----no vc-----")
            showThankyouPage()
            return
        }
        self.showNxtVC(nxVC: nxtVC)
    }
    
    func checkForProceed()->Bool{
        if  ((self.questData?.answer as! AnsRadioAndRating).selectedOption != nil){
                return true
            }
        return false
    }
    
    override func backAction() {
        if surveyCntr.isPreActive{
            surveyCntr.preIndex -= 1
            self.navigationController?.popViewController(animated: true)
        } else{
            surveyCntr.postIndex -= 1
            self.navigationController?.popViewController(animated: true)
        }
    }

}

extension RadioQuesVC {
    
    func setUpViews(){
        if SurveyController.shared.isPreActive{
            qNumLabel.text = "Q.\(SurveyController.shared.preIndex)"
        } else{
            qNumLabel.text = "Q.\(SurveyController.shared.postIndex)"
        }
        nextBtn.isEnabled = false
        nextBtn.setTitleColor(UIColor(rgbString: ColorType.disableBtn.rawValue), for: .disabled)
        nextBtn.setTitleColor(.white, for: .normal)
        nextBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
    }
    
    func enableDisableProceedBtn() {
        if ((self.questData?.answer as! AnsRadioAndRating).selectedOption) != nil {
            self.nextBtn.isEnabled = true
            return
        }
        nextBtn.isEnabled = false
    }
    
    func handleData(){
        if  surveyCntr.isPreActive{
            questData = surveyCntr.surveyContainer.preSurvey[surveyCntr.preIndex]
            self.surveyCntr.preIndex += 1
        }else{
            questData = surveyCntr.surveyContainer.postSurvey[surveyCntr.postIndex]
            self.surveyCntr.postIndex += 1
        }
        self.questStatementLabel.text = questData?.statement
    }
    
    func showThankyouPage(){
        print("-----no vc-----")
        let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNxtVC(nxVC: String){
        let vc = storyboard!.instantiateViewController(withIdentifier: nxVC)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension RadioQuesVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.questData?.optionAry.count else {
            return 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let option = questData?.optionAry[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = option?.statement
        let ans = self.questData?.answer as! AnsRadioAndRating
        cell.textLabel?.numberOfLines = 0

        if ans.selectedOption == indexPath.row{
//            let imgView = UIImageView(image: #imageLiteral(resourceName: "checkMark"))
            let imgView = UIImageView(image: getImage(name: "checkMark"))
            imgView.backgroundColor = .clear
            cell.accessoryView = imgView
            cell.textLabel?.textColor = UIColor(rgbString: ColorType.yellow.rawValue)
            cell.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue, alpha: 0.2)
            cell.textLabel?.backgroundColor = .clear
        } else {
            cell.accessoryView = nil
            cell.textLabel?.textColor = .white
            cell.backgroundColor = .clear
        }
        self.enableDisableProceedBtn()
        return cell
    }

    
}

extension RadioQuesVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
        let ansObj = self.questData?.answer as! AnsRadioAndRating
        ansObj.selectedOption = indexPath.row
        tableView.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
}

