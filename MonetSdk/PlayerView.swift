//
//  PlayerView.swift
//  MonetSDK
//
//  Created by Monet  on 19/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit
import AVKit

class PlayerView: UIView {
    
    /* --------------------------------------------------------------------------
     Properties
     -----------------------------------------------------------------------------*/
    let bufferingView: UIActivityIndicatorView = UIActivityIndicatorView()

    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer {
            return layer as! AVPlayerLayer
    }
    
    //  ---  Change layer class to AVPlayerLayer
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    
    /* --------------------------------------------------------------------------
     Initializers
     -----------------------------------------------------------------------------*/
    override init(frame: CGRect) {
        super.init(frame: frame)
        funcAtStartUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
        funcAtStartUp()
    }
    
    func funcAtStartUp(){
        setUpViews()
    }
    
    func setUpViews(){
        self.addSubview(bufferingView)
        bufferingView.translatesAutoresizingMaskIntoConstraints = false
        bufferingView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        bufferingView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true

    }
    
}

