//
//  ThankyouPageVC.swift
//  MonetSDK
//
//  Created by Monet  on 09/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit


class ThankyouPageVC: ViewControllerSdk {

    @IBOutlet weak var congratsLabel: UILabel!
    
    @IBOutlet weak var finishBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViews()
        setFinishFlag()
    }

    func setFinishFlag(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            SurveyController.shared.sendStagingApi(isCampaignCompleted: true)
        }
    }
//    override func viewDidAppear(_ animated: Bool) {
//        SurveyController.shared.sendStagingApi(isCampaignCompleted: true)
//
//    }
    
    func setViews(){
        congratsLabel.textColor = UIColor(rgbString: ColorType.yellow.rawValue)
        finishBtn.layer.cornerRadius = finishBtn.frame.height/2
        finishBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
    }
    @IBAction func finishAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    

    

}
