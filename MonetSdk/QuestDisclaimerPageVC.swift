//
//  QuestDisclaimerPageVC.swift
//  MonetSDK
//
//  Created by Monet  on 28/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class QuestDisclaimerPageVC: ViewControllerSdk {
    
    let surveyCntr = SurveyController.shared
//    private var stagingKey: Int{
//        get{
//            if SurveyController.shared.isPreActive{
//                return 2
//            }
//            return 3
//        }
//    }
    
    @IBOutlet weak var proceedBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        setUpViews()
    }
    
    /* --------------------------------------------------------
     Staging
     Update staging for pre and post campaigns respectively.
     --------------------------------------------------------*/
    
    override func viewDidAppear(_ animated: Bool) {
        if SurveyController.shared.isPreActive{
            SurveyController.shared.surveyContainer.stagingData.pre = stageStates.start
        } else{
            SurveyController.shared.surveyContainer.stagingData.post = stageStates.start
        }
        SurveyController.shared.sendStagingApi(isCampaignCompleted: false)

    }
    
    @IBAction func proceedBtnAction(_ sender: UIButton) {
        #warning("AKASH DO IT NOW: staging was here")
//        SurveyController.shared.stageUpdate(currentKey: self.stagingKey, value: 2, hitApiNow: true)
        guard let vcId = surveyCntr.nextSurveyVC() else {
            print("-----no vc-----")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = storyboard!.instantiateViewController(withIdentifier: vcId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func backAction() {
        self.quitAcion(quit: {
            #warning("AKASH DO IT NOW: staging was here")

//            SurveyController.shared.stageUpdate(currentKey: self.stagingKey, value: 3, hitApiNow: true)
        }, cancel: nil)
    }

    func setUpViews(){
        proceedBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        proceedBtn.layer.cornerRadius = proceedBtn.frame.height/2
    }
    
}
