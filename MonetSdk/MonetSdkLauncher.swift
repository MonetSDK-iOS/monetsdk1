//
//  MonetSdkLauncher.swift
//  MonetSDK
//
//  Created by Monet  on 26/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import Foundation

/* ----------------------------------------------------------------------------------------------------
 1. This class will be initantiated by our client.
 2. During instantiation the client will provied the following data:- (i) licence, (ii) userId, (iii) survayId/campaignId (iv) appLogo(optional) default value is blank image, (v) appThemeType(Optional), default is MonetTheme -------------------------------------------------------------------------------------------------*/

public protocol MonetSdkDelegate {
    func handleLaunchFailure(success: Bool, error: String?)
}

public class MonetSdkLauncher {
    
    public var delegate: MonetSdkDelegate!
//    private lazy var  storyBoard = UIStoryboard(name: "Sdssk", bundle: Bundle(identifier: "com.monetnetworks.MonetSdk"))
    private lazy var  storyBoard = UIStoryboard(name: "Sdk", bundle: Bundle(for: MonetSdkLauncher.self))



    public init(licence: String, userId: String, userAge: String, userGeneder: String, appLogo: UIImage?, theme: appThemeType?) {
        AppInjectedData.shared.licence = licence
        AppInjectedData.shared.userId = userId
        AppInjectedData.shared.userGender = userGeneder
        AppInjectedData.shared.userAge = userAge
        AppInjectedData.shared.appLogoImg = appLogo ?? UIImage()
        if let themeSelected = theme{
            AppInjectedData.shared.sdkTheme = themeSelected
        }
    }
    
    
    public func launch(from appVC: UIViewController, campaignId: String )  {
        //        showLoader(onVC: appVC)
        AppInjectedData.shared.surveyId = campaignId
        //----Call hitMonetLaunchPadApi() and after its success call following func to present sdk----
        
        let header = ["licence-key":AppInjectedData.shared.licence]
        let body = ["cmp_id":"\(AppInjectedData.shared.surveyId)", "user_id":"\(AppInjectedData.shared.userId)", "gender": AppInjectedData.shared.userGender, "age": "\(AppInjectedData.shared.userAge)"]
        let bodyJsonData = try! JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
        let api = ApiRequest(type: .post, endUrl: ApiEndPoinds().lauchPad, para: nil, body: bodyJsonData, authToken: nil, otherHedersKeyValue: header)
        api.hit {
            (respData) in
            if let data = respData{
//                print("api data recieved")
                //                    throw MonetErrors.monetServerNotResponding
                do{
                    let obj = try JSONDecoder().decode(ApiDataModel.self, from: data)
               
                    if obj.code == 200{
//                        print("success")
                        SurveyController.shared.surveyContainer = SurveyContainer(apiData: obj)
                        let vc = self.storyBoard.instantiateViewController(withIdentifier: "MonetSdkNavCntr") as! MonetSdkNavCntr
                        DispatchQueue.main.async {
                        appVC.present(vc, animated: true, completion: nil)
                        }
//                        self.hideLoader(fromVC: appVC)
                        self.delegate.handleLaunchFailure(success: true, error: nil)

                    } else{
//                        print("failed")
//                        self.hideLoader(fromVC: appVC)
                        self.delegate.handleLaunchFailure(success: false, error: obj.message)
                    }
                } catch{
                    print(error)
                    print("can't convert data into foundation object ")
//                    print(error)
//                    self.hideLoader(fromVC: appVC)
                    self.delegate.handleLaunchFailure(success: false, error: MonetErrors.unkownResponseFromMonerServer.rawValue)
                }
            } else{
                print("no response getting  from monetSdk api")
//                self.hideLoader(fromVC: appVC)
                self.delegate.handleLaunchFailure(success: false, error: MonetErrors.monetServerNotResponding.rawValue)
            }
        }

//        print("lauching sdk screens")
    }
    
//    private func showLoader(onVC: UIViewController){
//        DispatchQueue.main.async {
//            print("*****************laf*******************************************************")
////            let loaderVC = UIViewController()
////            loaderVC.modalPresentationStyle = .overCurrentContext
////            loaderVC.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
////            onVC.present(loaderVC, animated: false, completion: nil)
//        }
//
//    }
//    private func hideLoader(fromVC: UIViewController){
//        DispatchQueue.main.async {
////        fromVC.dismiss(animated: false, completion: nil)
//    }
//    }
}
