//
//  AnswerSender.swift
//  MonetSDK
//
//  Created by Monet  on 13/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//



/* -------------------------------------------------------------------------------------------
Get survey Answers, converts them into json and sends to the server.
 --------------------------------------------------------------------------------------------*/
class AnswerSender {
    
    var questArray = [Quest]()
    var qIdAnsDic: [String:Any] = [:]
    var surveyTyp: SurveySequence? = nil
    
    func processSurvey(surveyType: SurveySequence)  {
        qIdAnsDic = [:]
        self.surveyTyp = surveyType
        if surveyType == .pre{
            questArray = SurveyController.shared.surveyContainer.preSurvey
        } else if surveyType == .post{
            questArray = SurveyController.shared.surveyContainer.postSurvey
        }
            converter()
    }

    private func converter(){
        for question in questArray{
            switch question.type{
            case .grid:
                getGridData(quest: question)
            case .single:
                getRadioData(quest: question)
            case .multiple:
                getChecBoxData(quest: question)
            case .numeric:
                getRatingData(quest: question)
            case .freetext:
                getTextData(quest: question)
//            case .logicalWithChildQuest:
//                print("This question type is not valid for SDK")
            case .image:
                getRadioData(quest: question)
                print("This question type is not valid for SDK")
            case .multiple_image:
                getChecBoxData(quest: question)
                print("This question type is not valid for SDK")
//            case .undefined:
//                print("This question type is not valid for SDK")
            }
        }
        sendToApi()
    }
    
    private func getGridData(quest: Quest){
        let type = "\(quest.type.rawValue)"
        var ans: [String:String] = [:]
        let ansForCalculation = quest.answer as! AnsGrid
        var count = 0
        for opt in quest.optionAry{
            let ansIndex = ansForCalculation.selectedAnsArry[count]!
            let selectedSubAnwId = opt.gridOptionArray![ansIndex].id
            ans["\(opt.id)"] = "\(selectedSubAnwId)"
            count += 1
        }
        let addToFinal: [String:Any] = ["options":ans, "type":type]
        self.qIdAnsDic[String(quest.id)] = addToFinal
    }
    
    private func getRadioData(quest: Quest){
        let type = "\(quest.type.rawValue)"
        let ansForCalculation = quest.answer as! AnsRadioAndRating
        guard let ansIndex = ansForCalculation.selectedOption else {
            return
        }
        let ans = (quest.optionAry[ansIndex]).id
        let addToFinal: [String:Any] = ["options":ans, "type":type]
        self.qIdAnsDic[String(quest.id)] = addToFinal
    }
    private func getRatingData(quest: Quest){
        let type = "\(quest.type.rawValue)"
        guard let ans = (quest.answer as! AnsRadioAndRating).selectedOption else {
            return
        }
        let addToFinal: [String:Any] = ["options":ans, "type":type]
        self.qIdAnsDic[String(quest.id)] = addToFinal
    }
    
    
    private func getChecBoxData(quest: Quest){
        let type = "\(quest.type.rawValue)"
        let ansForCalculation = quest.answer as! AnsCheckBox
        var ans: [Int] = []
      
        for (key, value) in ansForCalculation.selectedOptions{
            if value{
                ans.append(quest.optionAry[key].id)
            }
        }
        let addToFinal: [String:Any] = ["selectedOptions":ans, "type":type]
        self.qIdAnsDic[String(quest.id)] = addToFinal
    }
    
    private func getTextData(quest: Quest){
        let type = "\(quest.type.rawValue)"
        let ans = (quest.answer as! AnsText).ansStringValue
        let addToFinal: [String:Any] = ["options":ans, "type":type]
        self.qIdAnsDic[String(quest.id)] = addToFinal
    }
    

    
    
    private func sendToApi(){
//        guard let type = self.surveyTyp else {
//            print("survey type not defined")
//            return
//        }
//        var ansDataToSend: String = ""
//        do{
//            let json = try JSONSerialization.data(withJSONObject: self.qIdAnsDic, options: .prettyPrinted)
//            ansDataToSend = String(data: json, encoding: .utf8) ?? ""
//            print(ansDataToSend)
//        } catch{
//            print(error)
//        }
//        let body: [String:Any] = ["cf_id": SurveyController.shared.surveyContainer.surveyResponseId,
//                                  "cmp_id":AppInjectedData.shared.surveyId,
//                                  "type":(type.rawValue).lowercased(),
//                                  "data":ansDataToSend
//                                  ]
        let bodyJsonData = try! JSONSerialization.data(withJSONObject: ["survey":self.qIdAnsDic], options: .prettyPrinted)
        let api = ApiRequest(type: .post,
                             endUrl: ApiEndPoinds().savePrePostAns,
                             para: nil,
                             body: bodyJsonData,
                             authToken: SurveyController.shared.surveyContainer.tokenForEndUser,
                             otherHedersKeyValue: ["licence-key":AppInjectedData.shared.licence])
        
         api.hit { (respData) in
            if let data = respData{
                do{
                    let resp = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                        print("-----response for sending survey answers api is = \(resp)----")

                } catch{
                    print("-----\(self.surveyTyp?.rawValue ?? "") survey answer api response can't be converted into json because:------")
                    print(error)
                }
               
            }else{
                print("-----\(self.surveyTyp?.rawValue ?? "") survey answer api fails-----")
            }
        }
    }
}


