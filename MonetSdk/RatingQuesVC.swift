//
//  RatingQuesVC.swift
//  MonetSDK
//
//  Created by Monet  on 08/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class RatingQuesVC: ViewControllerSdk {

    let surveyCntr = SurveyController.shared
    var questData: Quest?
    
    @IBOutlet weak var qNumLabel: UILabel!
    @IBOutlet weak var questStatementLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var ratingSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        handleData()
        setUpViews()
    }
    
    @IBAction func sliderAction(_ sender: SliderCustom) {
        nextBtn.isEnabled = true
        ratingSlider.value = roundf(sender.value)
        (questData?.answer as! AnsRadioAndRating).selectedOption = Int(sender.value)
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        guard let vcId = surveyCntr.nextSurveyVC() else {
            print("-----no vc-----")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = storyboard!.instantiateViewController(withIdentifier: vcId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func backAction() {
        if surveyCntr.isPreActive{
            surveyCntr.preIndex -= 1
            self.navigationController?.popViewController(animated: true)
        } else{
            surveyCntr.postIndex -= 1
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpViews(){
        if SurveyController.shared.isPreActive{
            qNumLabel.text = "Q.\(SurveyController.shared.preIndex)"
        } else{
            qNumLabel.text = "Q.\(SurveyController.shared.postIndex)"
        }
        nextBtn.isEnabled = false
        nextBtn.setTitleColor(UIColor(rgbString: ColorType.disableBtn.rawValue), for: .disabled)
        nextBtn.setTitleColor(.white, for: .normal)
        nextBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
    }
    
    func handleData(){
        if  surveyCntr.isPreActive{
            self.questData = surveyCntr.surveyContainer.preSurvey[surveyCntr.preIndex]
            self.questStatementLabel.text = self.questData?.statement
            self.surveyCntr.preIndex += 1
        }else{
            self.questData = surveyCntr.surveyContainer.postSurvey[surveyCntr.postIndex]
            self.questStatementLabel.text = self.questData?.statement
            self.surveyCntr.postIndex += 1
        }
    }

}
