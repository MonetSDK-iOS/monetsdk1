//
//  ReactionCollectCell.swift
//  MonetSDK
//
//  Created by Monet  on 22/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class ReactionCollectCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
