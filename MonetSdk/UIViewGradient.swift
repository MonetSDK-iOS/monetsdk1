//
//  UIViewGradient.swift
//  MonetSDK
//
//  Created by Monet  on 26/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//


/*------------------------------------------------------------------
 UIView subclass having main CALayer replaced with CAGradient Layer
 -------------------------------------------------------------------*/
class UIViewGradient: UIView {
    
    private let waterMark = UIImageView()
    
    
    override class var layerClass : AnyClass {
        switch AppInjectedData.shared.sdkTheme {
        case .monet:
            return CAGradientLayer.self
        case .gray:
            return CALayer.self
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setWaterMark()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setWaterMark()
    }
    

    
    private func setWaterMark (){
    }
    
    
}
