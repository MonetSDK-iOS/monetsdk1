//
//  InstructionsPageVC.swift
//  MonetSDK
//
//  Created by Monet  on 09/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class InstructionsPageVC: ViewControllerSdk {

    let surveyCntr = SurveyController.shared
//    private var emotionImgArry: [UIImage] = [UIImage(named: "Emotion1")!, UIImage(named: "Emotion2")!, UIImage(named: "Emotion3")!]
    private var emotionImgArry: [UIImage] = [getImage(name: "Emotion1"), getImage(name: "Emotion2"), getImage(name: "Emotion3")]

    private var emotionFirstTextArry: [String] = ["Allow your Camera", "Face camera without covering your face", "Be in Good lighting"]
     private var emotionSecondTextArry: [String] = ["", "", ""]
    
//    private var reactionImgArry: [UIImage] = [UIImage(named: "Reaction1")!, UIImage(named: "Reaction2")!, UIImage(named: "Reaction3")!]
    private var reactionImgArry: [UIImage] = [getImage(name: "Reaction1"), getImage(name: "Reaction2"), getImage(name: "Reaction3")]

    private var reactionFistTextArry: [String] = ["REACTION", "While playing video, click these icons to share your reactions", "While playing video, click these icons to share your reactions"]
    private var reactionSecondTextArry: [String] = ["In order to receive your rewards, you must follow the instructions", "", "Type your answer in the box."]
    
    
    @IBOutlet weak var pageCntr: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewDidLoad")
        setViews()
        collectionView.dataSource = self
        collectionView.delegate = self

    }

    override func viewDidAppear(_ animated: Bool) {

        var key = 5 //--- For post sruvey
        if SurveyController.shared.isEmotionActive{
            key = 4 //--- For pre sruvey
        }
        #warning("AKASH: staging was here")
//        SurveyController.shared.stageUpdate(currentKey: key, value: 1, hitApiNow: false)
    }
    override func viewDidLayoutSubviews() {
        setLayout()
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        if surveyCntr.isEmotionActive{
//            SurveyController.shared.stageUpdate(currentKey: 4, value: 2, hitApiNow: false)
            #warning("AKASH : staging was here")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.FaceAdjustmentPageVC.rawValue)
            self.navigationController?.pushViewController(vc, animated: true)
        } else{
//            SurveyController.shared.stageUpdate(currentKey: 5, value: 2, hitApiNow: false)
            #warning("AKASH : staging was here")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ReactionPlayerVC.rawValue)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setLayout(){
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .horizontal
        layout.itemSize = collectionView.frame.size
        layout.minimumLineSpacing = 0
        
    }
    
    func setViews(){
        
        collectionView.isPagingEnabled = true
        collectionView.bounces = false
        collectionView.backgroundColor = .clear
        nextBtn.isHidden = true
    }
    
   


}

extension InstructionsPageVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if SurveyController.shared.isEmotionActive{
            return self.emotionFirstTextArry.count
        }
        return self.reactionFistTextArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if SurveyController.shared.isEmotionActive{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstructionsCollectCell1", for: indexPath) as! InstructionsCollectCell1
            cell.firstLabel.text = emotionFirstTextArry[indexPath.item]
            cell.secondLabel.text = ""
            cell.imageView.image = emotionImgArry[indexPath.item]
            cell.backgroundColor = .clear
            return cell
        }
        if indexPath.item == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstructionsCollectCell1", for: indexPath) as! InstructionsCollectCell1
            cell.firstLabel.text = reactionFistTextArry[0]
            cell.secondLabel.text = reactionSecondTextArry[0]
            cell.imageView.image = reactionImgArry[0]
            cell.backgroundColor = .clear
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstructionsCollectCell2", for: indexPath) as! InstructionsCollectCell2
        cell.firstLabel.text = reactionFistTextArry[indexPath.item]
        cell.secondLabel.text = reactionSecondTextArry[indexPath.item]
        cell.imageView.image = reactionImgArry[indexPath.item]
        cell.backgroundColor = .clear
        return cell
    }
    
    
}


extension InstructionsPageVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
     let visibleCellNum = Int((collectionView.visibleCells[0].frame.origin.x)/(collectionView.frame.width))
            self.pageCntr.currentPage = visibleCellNum
        
        if visibleCellNum == 2{
            self.nextBtn.isHidden = false
        } else{
            self.nextBtn.isHidden = true
        }
    }
}


