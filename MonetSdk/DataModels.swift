//
//  OpenData.swift
//  MonetSDK
//
//  Created by Monet  on 28/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//


/*--------------------------------------------------------------------------------
 Staging classes and types
 --------------------------------------------------------------------------------*/
class stages: Encodable{
    private let device = "iOS"
    var landingPage: stageStates? = nil
    var pre: stageStates? = nil
    var post: stageStates? = nil
    var reaction: [String:stageStates]? = nil
    var detection: [String:stageStates]? = nil
    
    enum CodingKeys: CodingKey {
        case device
        case landingPage
        case pre
        case post
        case reaction
        case detection
    }
}

enum stageStates:String, Encodable{
    case start
    case complete
    case exit
}


/*--------------------------------------------------------------------
 Error types for SDK
 --------------------------------------------------------------------*/
public enum MonetErrors: String {
    case unkownResponseFromMonerServer
    case networkError
    case monetServerNotResponding
}

/*--------------------------------------------------------------------
 Helpers for "Quest" class
 --------------------------------------------------------------------*/

enum QuestType: String, Codable {
    case single
    case multiple
    case numeric
    case freetext
    case grid
    case image
    case multiple_image
}

/*-----------------------------------
 Data provided from client's App
 -----------------------------------*/

class AppInjectedData {
    static let shared = AppInjectedData(client: "", userId: "", userAge: "", userGender: "", surveyId: "", appLogo: nil)
    var sdkTheme: appThemeType = .monet
    var surveyId: String
    var licence: String
    var userId: String
    var userAge: String
    var userGender: String
    var appLogoImg: UIImage

    init(client: String, userId: String, userAge: String, userGender: String, surveyId: String , appLogo: UIImage? ) {
        self.licence = client
        self.userId = userId
        self.userAge = userAge
        self.userGender = userGender
        self.surveyId = surveyId
        self.appLogoImg = appLogo ?? UIImage()
    }
}

/*-----------------------------------
 Color themes for sdk
 -----------------------------------*/

public enum appThemeType {
    case monet
    case gray
}


/*-----------------------------------
 Survey Types
 -----------------------------------*/

 enum SurveySequence: String, Codable {
    case pre
    case post
    case emotion
    case reaction
}



/*-------------------------
 Question model
 -------------------------*/

class Quest {
    
    //---Selection of quest type will be handled here---
    var isAnswered: Bool = false
    var answer = Answer()
    var type: QuestType
    var id: Int
    var statement: String
    var optionAry: [OptionElement] = []
//    private var gridArray: ApiValues? = nil
    
    //---optionArray will not be available in case of text type ques---
    init(qType: QuestType, qId:Int, qStatement: String, optionArray: [ApiOptions]?, gridValueArray: [ApiValues]?) {
//        if let typ = QuestType(rawValue: qType){
            self.type = qType
            switch qType{
            case .grid:
                self.answer = AnsGrid()
            case .single:
                self.answer = AnsRadioAndRating()
            case .multiple:
                self.answer = AnsCheckBox(apiOptAry: optionArray ?? [])
            case .numeric:
                self.answer = AnsRadioAndRating()
            case .freetext:
                self.answer = AnsText()
            case .image:
                self.answer = AnsRadioAndRating()
            case .multiple_image:
                self.answer = AnsCheckBox(apiOptAry: optionArray ?? [])
      
        }
//                else {
//            self.type = .undefined
//        }
        self.type = qType
        self.id = qId
        self.statement = qStatement

        
        if let options = optionArray{
            for opt in options{
                let optElemnt = OptionElement(optionId: opt.opt_id, optionStr: opt.option_value, gridOptArry: gridValueArray)
                self.optionAry.append(optElemnt)
            }
        }
        
//        if let options = optionArray{
//            for opt in options{
//                let optElemnt = OptionElement(optionId: opt.opt_id, optionStr: opt.option_value, gridOptArry: opt.grid)
//                self.optionAry.append(optElemnt)
//            }
//        }
    }
}


/*----------------------------------------------------------------------------------------------
 Keep "Answer" super class blank, so that its subclasses has same type of variabel
 ----------------------------------------------------------------------------------------------*/

class Answer {
}

class AnsRadioAndRating: Answer/*--- 1 ---*/ {
    var selectedOption: Int? = nil
}

class AnsCheckBox: Answer/*--- 2 ---*/{
    var selectedOptions = [Int:Bool]()
    init(apiOptAry: [ApiOptions]) {
        for num in 0...apiOptAry.count-1{
            self.selectedOptions[num] = false
        }
    }
}

class AnsText: Answer/*--- 3 ---*/{
    var ansStringValue = String()
}

//class AnsDic: Answer/*--- 4 ---*/{
//    var selectedAns = [String:String]()
//}

class AnsGrid: Answer{
    var selectedAnsArry = [Int:Int]()
}

/*-------------------------
 Options model
 -------------------------*/

class OptionElement {
    
    var isSubQAnswered: Bool = false // --- for sub option --
    var subAnswer = AnsRadioAndRating() // --- for sub option --
    var id: Int
    var statement: String
    var gridOptionArray: [SubOptionElement]? = []
    
    init(optionId: Int, optionStr: String, gridOptArry: [ApiValues]?) {
        self.id = optionId
        self.statement = optionStr
        
        //------ Create Sub-Options, if "ApiGrid" passed -----
        if let gridOptAr = gridOptArry{
            for gridOpt in gridOptAr{
                let subOpt = SubOptionElement(optId: gridOpt.grid_id, optStr: gridOpt.grid_value)
                self.gridOptionArray?.append(subOpt)
            }
        }
        else {
            self.gridOptionArray = nil
        }
    }
}

/*-----------------------------------------
 Options model for Grid type subOptins
 -----------------------------------------*/

class SubOptionElement {
    var id: Int
    var statement: String
    init(optId: Int, optStr: String) {
        self.id = optId
        self.statement = optStr
    }
}

/*-----------------------------------------
 Emoji Ans model
 -----------------------------------------*/

struct ReactAns: Encodable {
    var Like: [EmojiAnsTimeComent]?
    var Love: [EmojiAnsTimeComent]?
    var Want: [EmojiAnsTimeComent]?
    var Memorable: [EmojiAnsTimeComent]?
    var Dislike: [EmojiAnsTimeComent]?
    var Accurate: [EmojiAnsTimeComent]?
    var Misleading: [EmojiAnsTimeComent]?
    var Engaging: [EmojiAnsTimeComent]?
    var Boring: [EmojiAnsTimeComent]?
    var Invalid: [EmojiAnsTimeComent]?
}

struct EmojiAnsTimeComent: Encodable {
    var time: Int
    var coment: String
}

enum EmojiEncoding: String, Codable{
    
    case Like
    case Love
    case Want
    case Memorable
    case Dislike
    case Accurate
    case Misleading
    case Engaging
    case Boring
    
}


