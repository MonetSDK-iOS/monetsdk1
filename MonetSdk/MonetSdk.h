//
//  MonetSdk.h
//  MonetSdk
//
//  Created by Monet  on 01/05/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MonetSdk.
FOUNDATION_EXPORT double MonetSdkVersionNumber;

//! Project version string for MonetSdk.
FOUNDATION_EXPORT const unsigned char MonetSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MonetSdk/PublicHeader.h>


