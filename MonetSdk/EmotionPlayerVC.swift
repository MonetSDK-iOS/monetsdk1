//
//  EmotionPlayerVC.swift
//  MonetSDK
//
//  Created by Monet  on 09/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit
import Photos
import CoreImage
import HaishinKit

 class EmotionPlayerVC: ViewControllerSdk {
    
    var rtmpStream: RTMPStream!
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureSession: AVCaptureSession? = AVCaptureSession()
    var frontCamera: AVCaptureDevice?
    let videoOuput = AVCaptureVideoDataOutput()
    let videoUrl = SurveyController.shared.surveyContainer.videoUrl
//    let videoUrl = URL(string: "http://dev.monetrewards.com/uploads/netflix/campaign_video/2079.mp4")
    var timeObserver: Any!
    var isVideoPlaybackStarted = false

    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var timeDurationLabel: UILabel!
    @IBOutlet weak var facePreviewView: UIView!
    @IBOutlet weak var detectionStatusView: UIView!
    @IBOutlet weak var notDetectiongLabel: UILabel!
    @IBOutlet weak var currentDurationLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
     }
    override func viewDidAppear(_ animated: Bool) {
        rtmpStartStream()
        setUpPlayer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        rtmpStopStream()
    }
    
    /* --------------------------------------------------------
     Quit Survey
     If back btn pressed alert asks to quit or continue the survey
     --------------------------------------------------------*/
    override func backAction() {
        #warning("AKASH SAYS: Save staging data to server before quiting")
        playerView.player?.pause()
        let alertCntr = UIAlertController(title: "Warning!", message: "Quiting survey will affect your rewards. \n Still want to quit?", preferredStyle: .actionSheet)
        let quitAction = UIAlertAction(title: "Quit", style: .destructive){ (_) in
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Continue", style: .cancel){ (_) in
            self.playerView.player?.play()
        }
        alertCntr.addAction(quitAction)
        alertCntr.addAction(cancelAction)
        self.present(alertCntr, animated: true, completion: nil)
    }

}

extension EmotionPlayerVC{
    
    /* --------------------------------------------------------
     Views and others
     --------------------------------------------------------*/
    func setUpView(){
        
        // --- Temperarily hidden ---
        notDetectiongLabel.isHidden = true
        detectionStatusView.isHidden = true
        
        timeDurationLabel.backgroundColor = .clear
        timeDurationLabel.textColor = .white
        timeDurationLabel.text = SurveyController.shared.surveyContainer.videoDuration

        view.backgroundColor = .black
        detectionStatusView.layer.cornerRadius = detectionStatusView.frame.height/2
        
        notDetectiongLabel.backgroundColor = .clear
        notDetectiongLabel.textColor = .white
        notDetectiongLabel.textAlignment = .center
        
        progressView.progress = 0
        progressView.trackTintColor = UIColor(rgbString: ColorType.gray.rawValue)
        progressView.progressTintColor = UIColor(rgbString: ColorType.red.rawValue)
        
    }
    
    func nextVc(){
        guard let vcId = SurveyController.shared.nextSurveyVC() else {
            print("-----no vc-----")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = storyboard!.instantiateViewController(withIdentifier: vcId)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension EmotionPlayerVC{
    
    /* --------------------------------------------------------
     (1) Initialize player inside "playerView" class
     (2) set video duration label
     (3) call "setTimer"
     --------------------------------------------------------*/
    func setUpPlayer(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.utility).async {
            guard let url = self.videoUrl else {
                return
            }
            self.playerView.player = AVPlayer(url: url)
            DispatchQueue.main.async {
                self.playerView.player!.play()
                self.videoProgressUpdate()
            }
        }
    }
    
    
    func videoProgressUpdate(){
        // ----  Send Notification on video End  -----
        NotificationCenter.default.addObserver(self, selector: #selector(handleVideoEnd(note:)) , name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerView.player?.currentItem)

        // ----  Show loader if video buffer empty  -----
        self.timeObserver = playerView.player!.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 2), queue: DispatchQueue.main) { (currentPlayingTime) in
            guard self.playerView.player!.currentItem != nil else{
                print("no current item")
                return
            }
            
            if self.playerView.player!.currentItem!.isPlaybackBufferEmpty{
//                if !self.isVideoPlaybackStarted{
//                    self.isVideoPlaybackStarted = true
//                }
                //                print("isPlaybackBufferEmpty")
                self.activityIndicator.startAnimating()
                self.activityIndicator.isHidden = false
            } else if self.playerView.player!.currentItem!.isPlaybackLikelyToKeepUp{
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            } else if self.playerView.player!.currentItem!.isPlaybackBufferFull{
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                //                print("isPlaybackBufferFull")
            }
            
            // ----  Update video time label and progress view  -----
            let sec = Int(CMTimeGetSeconds(currentPlayingTime))
            self.currentDurationLabel.text = String(format: "%02d", sec/60)+":"+String(format: "%02d", sec%60)
            if let itemDuration = self.playerView.player!.currentItem?.duration{
                self.progressView.progress = Float(CMTimeGetSeconds(currentPlayingTime)/CMTimeGetSeconds(itemDuration))
            }
        }

    }
    
    @objc func handleVideoEnd(note: Notification){
//        playerView.player!.removeTimeObserver(self.timeObserver)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerView.player?.currentItem)
        print("Emotion player video ends")
        self.rtmpStopStream()
        DispatchQueue.main.async {
            self.nextVc()
        }
    }

    
    
    /* --------------------------------------------------------
     RTMP Section
     --------------------------------------------------------*/
    func rtmpStartStream(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            print("rtmpStartStream func called")
            let rtmpConnection = RTMPConnection()
            self.rtmpStream = RTMPStream(connection: rtmpConnection)
            self.rtmpStream!.attachCamera(DeviceUtil.device(withPosition: .front)){
                error in
                print(error)
                print("camera coudn't be attached to the rtmp stream object")
                fatalError()
            }
//            self.rtmpStream!.orientation = .portrait
            //                print("hkview created")
       
            DispatchQueue.main.async {
                let hkView = HKView(frame: self.facePreviewView.bounds)
                //                hkView.orientation
                hkView.videoGravity = AVLayerVideoGravity.resizeAspectFill
                hkView.attachStream(self.rtmpStream!)
                self.facePreviewView.insertSubview(hkView, at: 0)
                rtmpConnection.connect("rtmp://app.monetrewards.com/Monet/", arguments: nil)
                self.publishRtmpNow()

            }
            // watch video at link : http://app.monetrewards.com/video_files/surveyResponseId.mp4
        }
    }
    
    func publishRtmpNow(){
        DispatchQueue.main.async {
            self.rtmpStream!.publish(String(SurveyController.shared.surveyContainer.surveyResponseId))
        }

    }
    
    func rtmpStopStream(){
        print("stoped streaming")
        rtmpStream?.close()
    }
    
    
    //    func getDelegatefForVideoBuffer(hView: HKView){
    //        guard let session = hView.layer.session else{
    //            return
    //        }
    //      session.beginConfiguration()
    //        if session.canAddOutput(self.videoOuput){
    //            session.addOutput(self.videoOuput)
    //            self.videoOuput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "myCustomSerialQueue"))
    //        }
    //        session.commitConfiguration()
    //    }
}
