//
//  GridCollectCell.swift
//  MonetSDK
//
//  Created by Monet  on 12/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class GridCollectCell: UICollectionViewCell {
    
    @IBOutlet weak var subQuetLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var questNumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      setUpView()
        
    }
    
    func setUpView(){
        subQuetLabel.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        questNumLabel.layer.cornerRadius = questNumLabel.frame.height/2
        questNumLabel.clipsToBounds = true
        questNumLabel.backgroundColor = UIColor(rgbString: ColorType.greenLight.rawValue)
    }
    
}
