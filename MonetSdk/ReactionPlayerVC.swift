//
//  ReactionPlayerVC.swift
//  MonetSDK
//
//  Created by Monet  on 09/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit
import AVFoundation

class ReactionPlayerVC: ViewControllerSdk {
    
    var currentEmojiOnDisplay = EmojiEncoding.Love
    var selectedEmojis = ReactAns()
    let videoUrl = SurveyController.shared.surveyContainer.videoUrl
//    let videoUrl = URL(string: "http://dev.monetrewards.com/uploads/netflix/campaign_video/2079.mp4")

    @IBOutlet weak var commentWindow: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var emojiImgViewForCommentView: UIImageView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var collectinView: UICollectionView!
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var timeDurationLabel: UILabel!
    @IBOutlet weak var currentDurationLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectinView.dataSource = self
        collectinView.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        commentWindow.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpPlayer()
        setUpView()
    }
    

    @IBAction func submitAction(_ sender: UIButton) {
        handleSubmit()
    }
    
 
    override func viewDidLayoutSubviews() {
//        setUpView()
    }
    
    /* --------------------------------------------------------
     Staging
     If back btn pressed alert asks to quit or continue the survey
     If quits Staging api will be hit
     --------------------------------------------------------*/
    override func backAction() {
        #warning("AKASH SAYS: Save staging data to server before quiting")
        playerView.player?.pause()
        let alertCntr = UIAlertController(title: "Warning!", message: "Quiting survey will affect your rewards. \n Still want to quit?", preferredStyle: .actionSheet)
        let quitAction = UIAlertAction(title: "Quit", style: .destructive){ (_) in
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Continue", style: .cancel){ (_) in
            self.playerView.player?.play()
        }
        alertCntr.addAction(quitAction)
        alertCntr.addAction(cancelAction)
        self.present(alertCntr, animated: true, completion: nil)
    }
}

extension ReactionPlayerVC{
    
    /* --------------------------------------------------------
     Views and others
     --------------------------------------------------------*/
    func setUpView(){
        
        commentWindow.backgroundColor = UIColor(rgbString: "#000000", alpha: 0.6)
        emojiImgViewForCommentView.backgroundColor = .white
        emojiImgViewForCommentView.layer.cornerRadius = emojiImgViewForCommentView.frame.height/2
        emojiImgViewForCommentView.clipsToBounds = true
        
        submitBtn.layer.cornerRadius = submitBtn.frame.height/2
        submitBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        
        playerView.backgroundColor = .clear
        collectinView.backgroundColor = .clear
        
        timeDurationLabel.backgroundColor = .clear
        timeDurationLabel.textColor = .white
        timeDurationLabel.text = SurveyController.shared.surveyContainer.videoDuration
        
        view.backgroundColor = .black
        
        progressView.progress = 0
        progressView.trackTintColor = UIColor(rgbString: ColorType.gray.rawValue)
        progressView.progressTintColor = UIColor(rgbString: ColorType.red.rawValue)
        
        guard let layout = collectinView.collectionViewLayout as? UICollectionViewFlowLayout else {
            print("no layout found")
            return
        }
        layout.itemSize.height = collectinView.frame.height/3 - 5.4
        layout.itemSize.width = collectinView.frame.width/3 - 5.4
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    

    
    func nextVc(){
        guard let vcId = SurveyController.shared.nextSurveyVC() else {
            print("-----no vc-----")
            let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = storyboard!.instantiateViewController(withIdentifier: vcId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
//    /* --------------------------------------------------------
//     Duration converter
//     --------------------------------------------------------*/
//    func secondsToHrsMinSec (seconds : Int) -> (String) {
//        let hrs = seconds / 3600
//        let min = (seconds % 3600) / 60
//        let sec = (seconds % 3600) % 60
//        if hrs < 1{
//            return "\(min):\(sec)"
//        }
//        return "\(hrs):\(min):\(sec)"
//    }
    
}

extension ReactionPlayerVC{
    
    /* --------------------------------------------------------
     (1) Initialize player inside "playerView" class
     (2) set video duration label
     (3) call "setTimer"
     --------------------------------------------------------*/
    func setUpPlayer(){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async {
            guard let url = self.videoUrl else {
                print("video url is not valid")
                fatalError()
            }
            self.playerView.player = AVPlayer(url: url)
            DispatchQueue.main.async {
                self.playerView.player?.play()
                self.videoProgressUpdate()
            }
        }
    
    }
    
    func videoProgressUpdate(){
        // ----  Send Notification on video End  -----
        NotificationCenter.default.addObserver(self, selector: #selector(handleVideoEnd(note:)) , name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerView.player?.currentItem)
        //        playerProgessUpdate()
        
        // ----  Show loader if video buffer empty  -----
        playerView.player!.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 2), queue: DispatchQueue.main) { (currentPlayingTime) in
            guard self.playerView.player!.currentItem != nil else{
                print("no current item")
                return
            }
            if self.playerView.player!.currentItem!.isPlaybackBufferEmpty{
                self.activityIndicator.startAnimating()
                self.activityIndicator.isHidden = false
            } else if self.playerView.player!.currentItem!.isPlaybackLikelyToKeepUp{
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            } else if self.playerView.player!.currentItem!.isPlaybackBufferFull{
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
            
            // ----  Update video time label and progress view  -----
            let sec = Int(CMTimeGetSeconds(currentPlayingTime))
            self.currentDurationLabel.text = String(format: "%02d", sec/60)+":"+String(format: "%02d", sec%60)
            if let itemDuration = self.playerView.player!.currentItem?.duration{
                self.progressView.progress = Float(CMTimeGetSeconds(currentPlayingTime)/CMTimeGetSeconds(itemDuration))
            }
        }
    }
    
    @objc func handleVideoEnd(note: Notification){
        print("video ends")
        self.sendReactionDataApi()
        DispatchQueue.main.async {
            self.nextVc()
        }
    }
}


extension ReactionPlayerVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SurveyController.shared.surveyContainer.emojiArrayForReaction.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReactionCollectCell", for: indexPath) as! ReactionCollectCell
        cell.backgroundColor = .clear
        cell.label.text = (SurveyController.shared.surveyContainer.emojiArrayForReaction[indexPath.item]).rawValue
//        cell.imageView.image = UIImage(named: (SurveyController.shared.surveyContainer.emojiArrayForReaction[indexPath.item]).rawValue)!
        cell.imageView.image = getImage(name: (SurveyController.shared.surveyContainer.emojiArrayForReaction[indexPath.item]).rawValue)
        return cell
    }
    
    
}

extension ReactionPlayerVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let emojiName = SurveyController.shared.surveyContainer.emojiArrayForReaction[indexPath.item].rawValue
        self.currentEmojiOnDisplay = EmojiEncoding(rawValue: emojiName)!
        self.playerView.player?.pause()
//        self.emojiImgViewForCommentView.image = UIImage(named: emojiName)!
        let bundle = Bundle(for: type(of: self).self)
        self.emojiImgViewForCommentView.image = UIImage(named: emojiName, in: bundle, compatibleWith: nil)!
        self.commentWindow.isHidden = false
        self.textView.becomeFirstResponder()
        self.textView.text = ""
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func handleSubmit(){
        var time: Double? = nil
        time = (self.playerView.player?.currentTime())?.seconds
        guard let timeStamp = time else {
            print("time not availabel for the video")
            return
        }
        let new = EmojiAnsTimeComent(time: Int(round(timeStamp)), coment: self.textView.text)
        
        switch currentEmojiOnDisplay {
        case .Like:
            if self.selectedEmojis.Like != nil{
                self.selectedEmojis.Like?.append(new)
            } else{
                self.selectedEmojis.Like = [new]
            }
        case .Love:
            if self.selectedEmojis.Love != nil{
                self.selectedEmojis.Love?.append(new)
            } else{
                self.selectedEmojis.Love = [new]
            }
        case .Want:
            if self.selectedEmojis.Want != nil{
                self.selectedEmojis.Want?.append(new)
            } else{
                self.selectedEmojis.Want = [new]
            }
        case .Memorable:
            if self.selectedEmojis.Memorable != nil{
                self.selectedEmojis.Memorable?.append(new)
            } else{
                self.selectedEmojis.Memorable = [new]
            }
        case .Dislike:
            if self.selectedEmojis.Dislike != nil{
                self.selectedEmojis.Dislike?.append(new)
            } else{
                self.selectedEmojis.Dislike = [new]
            }
        case .Accurate:
            if self.selectedEmojis.Accurate != nil{
                self.selectedEmojis.Accurate?.append(new)
            } else{
                self.selectedEmojis.Accurate = [new]
            }
        case .Misleading:
            if self.selectedEmojis.Misleading != nil{
                self.selectedEmojis.Misleading?.append(new)
            } else{
                self.selectedEmojis.Misleading = [new]
            }
        case .Engaging:
            if self.selectedEmojis.Engaging != nil{
                self.selectedEmojis.Engaging?.append(new)
            } else{
                self.selectedEmojis.Engaging = [new]
            }
        case .Boring:
            if self.selectedEmojis.Boring != nil{
                self.selectedEmojis.Boring?.append(new)
            } else{
                self.selectedEmojis.Boring = [new]
            }
        }
        self.commentWindow.isHidden = true
        self.textView.resignFirstResponder()
        self.playerView.player?.play()
        self.navigationController?.navigationBar.isHidden = false
    }
    
}

extension ReactionPlayerVC{
    
    /*----------------------------------------------------------------
     API
---------------------------------------------------------------- */
    func sendReactionDataApi(){
        let otherHeaders: [String:String] = ["licence-key":AppInjectedData.shared.licence]
        let body = ["reaction_data":self.selectedEmojis]
        let bodyJsonData = try! JSONEncoder().encode(body)
        let api = ApiRequest(type: .post, endUrl: ApiEndPoinds().saveReactions, para: nil, body: bodyJsonData, authToken: SurveyController.shared.surveyContainer.tokenForEndUser, otherHedersKeyValue: otherHeaders)
        api.hit { (data) in
            if let resp = data{
                do{
                    let response = try JSONSerialization.jsonObject(with: resp, options: [])
                    print(response)
                }catch{
                    print(error)
                }
            }
        }
    }
}

