//
//  FaceAdjustmentPageVC.swift
//  MonetSDK
//
//  Created by Monet  on 09/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit
import Photos
import CoreImage

class FaceAdjustmentPageVC: ViewControllerSdk {
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureSession: AVCaptureSession?
    var frontCamera: AVCaptureDevice?
    let videoOuput = AVCaptureVideoDataOutput()
    var isFaceDetecting = false
    var count = 3
    var timer: Timer?

    @IBOutlet weak var bottomDetectionView: UIView!
    @IBOutlet weak var detectionStatusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        if prepare(){
            self.captureSession?.startRunning()
            do{
                try self.displayPreview(on: self.view)
            } catch{
                print(error)
            }
        }
        setTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("********************************")
    }
    
    func setTimer(){
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (tmr) in
            print("timer running")
            self.count -= 1
            if self.isFaceDetecting && self.count == 0{
                DispatchQueue.main.async {
                    let someView = UIView(frame: self.bottomDetectionView.bounds)
                    someView.backgroundColor = UIColor(rgbString: ColorType.greenLight.rawValue)
                    self.bottomDetectionView.addSubview(someView)
                    let someLabel = UILabel(frame: self.detectionStatusLabel.frame)
                    someView.addSubview(someLabel)
                    someLabel.textColor = .white
                    someLabel.textAlignment = .center
                    someLabel.text = "Detected"
                    self.captureSession?.stopRunning()
                    self.captureSession = nil
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1, execute: {
                    tmr.invalidate()
                    print("timer invalidated")
                    self.timer = nil
                    self.nextVC()

                })
            }
        }
    }
    
    func prepare() -> Bool {
        do {
            self.createCaptureSession()
            try self.configureCaptureDevices()
            try self.configureDeviceInputs()
            try self.configureVideoOutput()
        }
        catch {
            print(error)
            return false
        }
        return true
    }
    
    func displayPreview(on theview: UIView) throws {
        guard let captureSession = self.captureSession, captureSession.isRunning else {
            print("no session available")
            return
        }        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.previewLayer?.connection?.videoOrientation = .portrait
        self.previewLayer?.frame = theview.frame
        theview.layer.insertSublayer(self.previewLayer!, at: 0)
    }
    

    @IBAction func nextDeleteThis(_ sender: UIButton) {
    
    }
    
    func nextVC(){
        let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.EmotionPlayerVC.rawValue)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension FaceAdjustmentPageVC {

    func createCaptureSession() /*----1----*/ {
        self.captureSession = AVCaptureSession()
    }
 
    func configureCaptureDevices() throws /*----2----*/ {
        let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        self.frontCamera = videoDevice
    }

    func configureDeviceInputs() throws /*----3----*/{
        let videoDeviceInput = try? AVCaptureDeviceInput(device: frontCamera!)
        self.captureSession?.beginConfiguration()
        if (captureSession?.canAddInput(videoDeviceInput!))!{
            self.captureSession?.addInput(videoDeviceInput!)
        }
        self.captureSession?.commitConfiguration()
    }
    
    func configureVideoOutput() throws /*----4----*/{
        self.captureSession?.beginConfiguration()
        if captureSession!.canAddOutput(videoOuput){
            self.captureSession!.addOutput(self.videoOuput)
        }
        videoOuput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: NSNumber(value: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
        videoOuput.alwaysDiscardsLateVideoFrames = true
        videoOuput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "myCustomSerialQueue"))
        self.captureSession?.sessionPreset = .high
        self.captureSession?.commitConfiguration()
        
    }

}


extension FaceAdjustmentPageVC:  AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        DispatchQueue.main.async {
            if connection.isVideoOrientationSupported{
                connection.videoOrientation = AVCaptureVideoOrientation.portrait
            }
            let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
            let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
//            let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
            let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyLow]
//            let option = [CIDetectorEyeBlink : true, CIDetectorSmile : true]
            let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
//            let faceArray = faceDetector?.features(in: ciimage, options: option)
//            let faceArray = faceDetector?.features(in: ciimage, options: nil)
            let faceArray = faceDetector?.features(in: ciimage)
            
            if let _ = faceArray?.first as? CIFaceFeature {
                print("FACE DETECTED")
               self.isFaceDetecting = true
                self.bottomDetectionView.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
                self.detectionStatusLabel.text = "Stay Steady..."
                
            } else {
                print("NO FACE")
                self.isFaceDetecting = false
                self.bottomDetectionView.backgroundColor = .gray
                self.detectionStatusLabel.text = "Adjust your face"
                self.count = 3
            }
        }
        
    }
    
   
}

