//
//  ImageRadioQuesVCViewController.swift
//  MonetSDK
//
//  Created by Monet  on 15/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class ImageRadioQuesVC: ViewControllerSdk {
    
    let surveyCntr = SurveyController.shared
    var questData: Quest?
    private var imgDataDic: [Int:UIImage] = [:]
    
    @IBOutlet weak var qNumLabel: UILabel!
    @IBOutlet weak var questStatementLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        handleData()
        
    }
    override func viewDidLayoutSubviews() {
        setUpViews()
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        guard let nxtVC = surveyCntr.nextSurveyVC() else {
            print("-----no vc-----")
            showThankyouPage()
            return
        }
        self.showNxtVC(nxVC: nxtVC)
    }
    
    override func backAction() {
        if surveyCntr.isPreActive{
            surveyCntr.preIndex -= 1
            self.navigationController?.popViewController(animated: true)
        } else{
            surveyCntr.postIndex -= 1
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
}

extension ImageRadioQuesVC {
    
    func enableDisableProceedBtn() {
        if ((self.questData?.answer as! AnsRadioAndRating).selectedOption) != nil {
            self.nextBtn.isEnabled = true
            return
        }
        nextBtn.isEnabled = false
    }
    
    func setUpViews(){
        if SurveyController.shared.isPreActive{
            qNumLabel.text = "Q.\(SurveyController.shared.preIndex)"
        } else{
            qNumLabel.text = "Q.\(SurveyController.shared.postIndex)"
        }
        nextBtn.isEnabled = false
        nextBtn.setTitleColor(UIColor(rgbString: ColorType.disableBtn.rawValue), for: .disabled)
        nextBtn.setTitleColor(.white, for: .normal)
        nextBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        nextBtn.layer.cornerRadius = nextBtn.frame.height/2
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = 3
        layout.scrollDirection = .vertical
        let itemWidth = (collectionView.bounds.width/3)-(layout.minimumInteritemSpacing*(2/3))
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
    }
    
    func handleData(){
        if  surveyCntr.isPreActive{
            questData = surveyCntr.surveyContainer.preSurvey[surveyCntr.preIndex]
            self.surveyCntr.preIndex += 1
        }else{
            questData = surveyCntr.surveyContainer.postSurvey[surveyCntr.postIndex]
            self.surveyCntr.postIndex += 1
        }
        self.questStatementLabel.text = questData?.statement
    }
    
    func showThankyouPage(){
        print("-----no vc-----")
        let vc = storyboard!.instantiateViewController(withIdentifier: viewCntrlIdentiers.ThankyouPageVC.rawValue)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNxtVC(nxVC: String){
        let vc = storyboard!.instantiateViewController(withIdentifier: nxVC)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ImageRadioQuesVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.questData?.optionAry.count else {
            return 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let option = questData?.optionAry[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collctCell", for: indexPath)
        let imgView = UIImageView()
        cell.contentView.addSubview(imgView)
//        imgView.image = #imageLiteral(resourceName: "ImageWaterMark")
        imgView.image = UIImage(named: "ImageWaterMark", in: Bundle(for: MonetSdkLauncher.self), compatibleWith: nil)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.topAnchor.constraint(equalTo: cell.contentView.topAnchor).isActive = true
        imgView.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor).isActive = true
        imgView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor).isActive = true
        imgView.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor).isActive = true
//        DispatchQueue.main
        if let img = self.imgDataDic[indexPath.item]{
            imgView.image = img
        } else{
            let imgUrlStr = option?.statement
            // download image and add to the dictionay according to index
        }
        let ans = self.questData?.answer as! AnsRadioAndRating
        if ans.selectedOption == indexPath.item{
            cell.contentView.layer.borderColor = UIColor.red.cgColor
            cell.contentView.layer.borderWidth = 2
        } else {
            cell.backgroundColor = .clear
            cell.contentView.layer.borderWidth = 0
        }
        self.enableDisableProceedBtn()
        return cell
    }
    

}

extension ImageRadioQuesVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let ansObj = self.questData?.answer as! AnsRadioAndRating
        ansObj.selectedOption = indexPath.item
        collectionView.reloadData()
    }
    
}
