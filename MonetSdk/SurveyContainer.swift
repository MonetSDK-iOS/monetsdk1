//
//  SurveyContainer.swift
//  MonetSDK
//
//  Created by Monet  on 27/03/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//


/*--------------------------------------------------------------------
 Model that contains all info about the survey; including selected answers
 --------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------
 Step 1 - To check which kind of survey LandingPageVC/other VC will start next, we need to first check nil value for "sequence" and then "sequence[surveyIndex]"
 step 2 - Just after starting the survey increment "surveyIndex" by "1"
 Step 3 - If pre/post found we lauch desclaimer VC for that and inside that vc we will check for quest type inside the "preSurvey[preIndex]/postSurvey[postIndex]" array & launch that type of VC. Here we also need to check this array max count before incrementing and checking the array value for next vc.
 Step 4 - Ans will be stored in this same array
 ----------------------------------------------------------------------------------------------*/

class SurveyContainer{
    var tokenForEndUser: String!
    var sequence = [SurveySequence]()
    var preSurvey = [Quest]()//---Will contain different types of ques---
    var postSurvey = [Quest]()//---Will contain different types of ques---
    var videoUrl: URL?
    var videoThumbnailUrl: URL?
    var surveyResponseId: Int!
//    var sruveyId = ""
    var emojiArrayForReaction: [EmojiEncoding] = []
    var numOfPreQ: Int {
        get {
            return preSurvey.count
        }
    }
    var numOfPostQ: Int{
        get {
            return postSurvey.count
        }
    }
    var campaignName: String?
    private var videoLength: Int?
    
    lazy var videoDuration: String = {
        guard let duration = self.videoLength else {
            print("*** No video duration found in survey container ***")
            return ""
        }
//        let hrs = ((duration/60)%60)
        return "\((duration/60)%60)"+":"+String(format: "%02d", duration%60)
//        return String(format: "%02d", (duration/60)%60)+":"+String(format: "%02d", duration%60)
    }()
    
    let stagingData = stages()
    
    
    /*------------------------------------------
     Initialiser: Initalised with "ApiDataModel"
     -------------------------------------------*/
    
    init(apiData: ApiDataModel) {
        guard let data = apiData.data else{
            print("*** No api data available to make survey container ***")
            return
        }
            self.sequence = data.sequence
            if let preData = data.pre{
                setSurvey(data: preData, type: .pre)
            }
            if let postData = data.post{
                setSurvey(data: postData, type: .post)
            }
            self.surveyResponseId = data.user_response_id
            self.videoUrl = URL(string: data.content_url)
            self.videoThumbnailUrl = URL(string: data.thumb_url)
            self.tokenForEndUser = data.token

        if let eomjiArray = data.reaction_inputs{
            self.emojiArrayForReaction = eomjiArray
        }
            self.campaignName = data.cmp_name
        self.videoLength = data.content_length
        
    }
    
 
    
    func setSurvey(data: ApiQuestions, type survey : SurveySequence)  {
        guard let qArray = data.questions else {print("no question provided"); return}
        
        //--------converting api quest into usabel quest model------
        for q in qArray{
            let quest = Quest(qType: q.questionType, qId: q.qs_id, qStatement: q.question, optionArray: q.options, gridValueArray: q.values)
            switch survey{
            case .pre:
                self.preSurvey.append(quest)
            case .post:
                self.postSurvey.append(quest)
            default :
                print("wrong call for survey creation")
            }
        }
    }
}
