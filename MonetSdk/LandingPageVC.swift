//
//  LandingPageVC.swift
//  MonetSDK
//
//  Created by Monet  on 26/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

import UIKit

class LandingPageVC: ViewControllerSdk {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var videoNameLabel: UILabel!
    @IBOutlet weak var termConditionBtn: UIButton!
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("monetId = \(SurveyController.shared.surveyContainer.surveyResponseId)")
        
//        getThumbnail()
        SurveyController.shared.capaignLaunched = true
        setUpViews()
        getThumbnail()
    }
    
    
    /*----------------------------------------------------------
     Staging
    ------------------------------------------------------------------*/
    override func viewDidAppear(_ animated: Bool) {
        SurveyController.shared.surveyContainer.stagingData.landingPage = stageStates.start
        SurveyController.shared.sendStagingApi(isCampaignCompleted: false)
    }
    
    deinit {
        //----------To Reset Survey data--------
        print("-------landing page vc distroyed------")
        SurveyController.shared.capaignLaunched = false
        SurveyController.shared = SurveyController()
    }
  
    @IBAction func termCondtionAction(_ sender: UIButton) {
        agreeBtn.isEnabled = !agreeBtn.isEnabled
        if agreeBtn.isEnabled{
            termConditionBtn.setImage(UIImage(named: "checkBoxFilled", in: Bundle(for: MonetSdkLauncher.self), compatibleWith: nil), for: .normal)
        } else{
            termConditionBtn.setImage(UIImage(named: "checkBox", in: Bundle(for: MonetSdkLauncher.self), compatibleWith: nil), for: .normal)
        }

    }
    
    @IBAction func agreeBtnAction(_ sender: Any) {
        guard let vcId = SurveyController.shared.nextSurveyVC() else {
            print("-----no vc-----")
            return
        }
        let vc = storyboard!.instantiateViewController(withIdentifier: vcId)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUpViews(){
//        if let thumbnail = SurveyController.shared.surveyContainer.videoThumbnail{
//            DispatchQueue.main.async {
//                self.videoImgView.image = thumbnail
//            }
//        }
//        videoImgView.image = SurveyController.shared.surveyContainer.videoThumbnail
        
        agreeBtn.isEnabled = false
        agreeBtn.setTitleColor(.white, for: .normal)
        agreeBtn.setTitleColor(UIColor(rgbString: ColorType.disableBtn.rawValue), for: .disabled)
        agreeBtn.backgroundColor = UIColor(rgbString: ColorType.blueDarkColor.rawValue)
        agreeBtn.layer.cornerRadius = agreeBtn.frame.height/2
        
        logoImgView.layer.cornerRadius = logoImgView.frame.height/2
        logoImgView.clipsToBounds = true
        logoImgView.backgroundColor = .gray
        
        videoNameLabel.text = SurveyController.shared.surveyContainer.campaignName ?? ""
        
        timeLabel.text = SurveyController.shared.surveyContainer.videoDuration+" Mins"
    }
    
 
    func getThumbnail () {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async {
            _ = ApiRequest(downloadImage: SurveyController.shared.surveyContainer.videoThumbnailUrl!, authToken: nil) { (imgData) in
                guard let data = imgData else {
                    return
                }
                guard let img = UIImage(data: data) else {
                    return
                }
                DispatchQueue.main.async {
                    self.videoImgView.image = img
                    //                    self.videoImgView.image = img
                }
            }
        }
    }
  
    /* ----------------------------------------------------------------------------------------------
     Quit Survey
     If user clicks the back button on this screen, options will be provided to either continue the survey or quite the survey.
    -------------------------------------------------------------------------------------------------- */
    
    override func backAction() {
        self.quitAcion(quit: {
            // ---- Updates the database for Staging.
            SurveyController.shared.surveyContainer.stagingData.landingPage = stageStates.exit
            SurveyController.shared.sendStagingApi(isCampaignCompleted: false)
        }, cancel: nil)

    }
    

    
    
}
