//
//  ExtensionsOfSwiftClasses.swift
//  MonetSDK
//
//  Created by Monet  on 27/02/19.
//  Copyright © 2019 MonetNetworks. All rights reserved.
//

//import Foundation


/* -----------------------------------
 UIColor
 ------------------------------------ */

extension UIColor{
    
    
    // Hex string to rgb color converter
    convenience init(rgbString: String){
        var cString:String = rgbString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
        
    }
    
    // Hex string to rgb color converter ---- with alpha variable ---
    convenience init(rgbString: String, alpha: CGFloat){
        var cString:String = rgbString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
        
    }
    
}


/* -----------------------------------
 UIViewController
 ------------------------------------ */

extension UIViewController{
    
    // ----------------------------- Alert 1 -------------------------
    func showAlert (alertTitle: String, msg: String){
        let alertCntr = UIAlertController(title: alertTitle, message: msg, preferredStyle: .alert)
        alertCntr.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alertCntr, animated: true, completion: nil)
    }
    
    // ----------------------------- Alert 2 -------------------------
    func showToast (alertTitle: String, msg: String){
        let alertCntr = UIAlertController(title: alertTitle, message: msg, preferredStyle: .alert)
        self.present(alertCntr, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            alertCntr.dismiss(animated: true, completion: nil)
        }
    }
    
}

//extension UIButton{
//
//    func disable(){
//        guard self.superview?.tag != 999 else {
//            print("alread disabled")
//            return
//        }
////        self.setTitleColor(.clear, for: .disabled)
//        let blurView = UIView()
////        self.addSubview(blurView)
//        self.insertSubview(blurView, at: 0)
//        blurView.translatesAutoresizingMaskIntoConstraints = false
//        blurView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
//        blurView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
//        blurView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        blurView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
//
//        blurView.backgroundColor = UIColor(white: 1, alpha: 0.5)
////        blurView.layer.cornerRadius = blurView.frame.height/2
//        self.clipsToBounds = true
//
////        blurView.clipsToBounds = true
//        blurView.tag = 999
//    }
//
//    func enable() {
//        if let blurView = self.viewWithTag(999){
//            blurView.removeFromSuperview()
//        }
//    }
//}

